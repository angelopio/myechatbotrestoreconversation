package mye.chatbotrestoreconversation.gcloud.bigquery;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryException;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.Dataset;
import com.google.cloud.bigquery.DatasetInfo;
import com.google.cloud.bigquery.Field;
import com.google.cloud.bigquery.LegacySQLTypeName;
import com.google.cloud.bigquery.Schema;
import com.google.cloud.bigquery.StandardTableDefinition;
import com.google.cloud.bigquery.Table;
import com.google.cloud.bigquery.TableDefinition;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableInfo;

public class DatasetDDLManager {

	private static BigQuery bigquery;

	private static final String DATASET_NAME = "myedataset_sessions";
	private static final String TABLE_NAME = "mye_sessions";

	static {
		// Instantiate a client.
		// If you don't specify credentials when constructing a client, the client library
		// will look for credentials in the environment, such as the
		// GOOGLE_APPLICATION_CREDENTIALS environment variable.
		bigquery = BigQueryOptions.getDefaultInstance().getService();
	}

	public static void main(String[] args) {

		// Step.1 Create dataset
		createDataset(DATASET_NAME);

		// Step.2 Create table
		// createTable(DATASET_NAME, TABLE_NAME);
	}

	public static String createDataset(String name) {

		// Prepares a new dataset
		Dataset dataset = null;
		DatasetInfo datasetInfo = DatasetInfo.newBuilder(name).build();

		// Creates the dataset
		try {
			dataset = bigquery.create(datasetInfo);
		} catch (BigQueryException ex) {
			System.out.println("**** BigQueryException ****");
			ex.printStackTrace();
			return null;
		}

		return dataset.getDatasetId().getDataset();
	}

	public static Table createTable(String datasetName, String tableName) {

		TableId tableId = TableId.of(datasetName, tableName);

		// Table fields definition
		Field sessionId = Field.of("sessionId", LegacySQLTypeName.STRING);
		Field userId = Field.of("userId", LegacySQLTypeName.STRING);
		Field timestamp = Field.of("timestamp", LegacySQLTypeName.TIMESTAMP);
		Field query = Field.of("query", LegacySQLTypeName.STRING);

		// Table schema definition
		Schema schema = Schema.of(sessionId, userId, timestamp, query);

		TableDefinition tableDefinition = StandardTableDefinition.of(schema);
		TableInfo tableInfo = TableInfo.newBuilder(tableId, tableDefinition).build();

		return bigquery.create(tableInfo);
	}
}