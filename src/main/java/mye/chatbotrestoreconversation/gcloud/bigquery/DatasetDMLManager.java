package mye.chatbotrestoreconversation.gcloud.bigquery;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.Table;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableResult;

public class DatasetDMLManager {

	private static BigQuery bigquery;

	private static final String DATASET_NAME = "myedataset_sessions";
	private static final String TABLE_NAME_ID = "mye_sessions";

	static {
		// Instantiate a client.
		// If you don't specify credentials when constructing a client, the client
		// library will look for credentials in the environment, such as the
		// GOOGLE_APPLICATION_CREDENTIALS environment variable.
		bigquery = BigQueryOptions.getDefaultInstance().getService();
	}

	public static void main(String[] args) {

		// Insert records into table
		// insertRecordsIntoTable(DATASET_NAME, TABLE_NAME_ID);

		// Read records from table
		try {
			loadRecordsFromTable();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static boolean insertRecordsIntoTable(String datasetName, String tableName) {

		String currentTimeMillis = String.valueOf(System.currentTimeMillis());
		long currentTime = Long.parseLong(currentTimeMillis.substring(0, currentTimeMillis.length()-3)); // For GCP dataset timestamp format

		TableId tableId = TableId.of(datasetName, tableName);

		// Values of the row to insert
		Map<String, Object> rowContent = new HashMap<>();
		rowContent.put("sessionId", "session_01");
		rowContent.put("userId", "user_01");
		rowContent.put("timestamp", currentTime);
		rowContent.put("query", "query_01");

		// More rows can be added in the same RPC by invoking .addRow() on the builder
		Table table = bigquery.getTable(tableId);
		InsertAllResponse response = bigquery.insertAll(InsertAllRequest.newBuilder(tableId).addRow("rowId", rowContent).build());

		// N.B. If any of the insertions failed, this lets you inspect the errors:
		// Es. for (Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) { // inspect row error }

		System.out.println("My table: " + table.getDescription());
		System.out.println("Response has errors: " + response.hasErrors());
		return response.hasErrors();
	}

	public static void loadRecordsFromTable() throws InterruptedException {

		// Use standard SQL syntax for queries. See: https://cloud.google.com/bigquery/sql-reference/
	    QueryJobConfiguration queryConfig =
	        QueryJobConfiguration.newBuilder(
	              "SELECT sessionId, userId, timestamp, query "
	              + "FROM `mye-chatbot-restoreconv.myedataset_sessions.mye_sessions` ").setUseLegacySql(false).build();

	    // Create a job ID so that we can safely retry.
	    JobId jobId = JobId.of(UUID.randomUUID().toString());
	    Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

	    // Wait for the query to complete.
	    queryJob = queryJob.waitFor();

	    // Check for errors
	    if (queryJob == null) {
	      throw new RuntimeException("Job no longer exists");
	    } else if (queryJob.getStatus().getError() != null) {
	      // You can also look at queryJob.getStatus().getExecutionErrors() for all
	      // errors, not just the latest one.
	      throw new RuntimeException(queryJob.getStatus().getError().toString());
	    }

	    // Get the results.
	    TableResult result = queryJob.getQueryResults();
	    // QueryResponse response = bigquery.getQueryResults(jobId); // Metodo alternativo

	    // Print all pages of the results.
	    for (FieldValueList row : result.iterateAll()) {
	      System.out.printf("sessionId: %s, userId: %s, timestamp: %s, query: %s",
	    		  row.get("sessionId").getStringValue(), row.get("userId").getStringValue(), row.get("timestamp").getStringValue(), row.get("query").getStringValue());
	    }
	}
}