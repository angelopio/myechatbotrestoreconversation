package mye.chatbotrestoreconversation.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

// Class will be at addressable at the URI "/helloworld"
@Path("/helloworld")
public class HelloWorldRest {

	@GET // The java method will process HTTP GET requests
	@Produces("text/plain") // The Java method will produce content identified by the MIME Media type "text/plain"
	public String getMessage() {
		return "Hello World RESTful with Jersey";
	}
}