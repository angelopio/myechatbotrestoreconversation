package mye.chatbotrestoreconversation.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import mye.chatbotrestoreconversation.rest.server.ChatbotSessionBean;

public class ChatbotRestoreConversationJerseyClient {

  private static final String REST_URI = "http://localhost:8080/api";
  private static Client client = ClientBuilder.newClient();

  public static ChatbotSessionBean getMyeSession(String sessionId) {

	  ChatbotSessionBean chatbotSessionBean = client.target(REST_URI).path(sessionId).request(MediaType.APPLICATION_XML).get().readEntity(ChatbotSessionBean.class);
	  return chatbotSessionBean;
  }
}