package mye.chatbotrestoreconversation.rest.client;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mye.chatbotrestoreconversation.rest.server.ChatbotSessionBean;

@WebServlet(
    name = "DatasetDMLManagerClientServlet",
    urlPatterns = {"/loadrecord"}
)

public class DatasetDMLManagerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    response.setContentType("text/plain");
    response.setCharacterEncoding("UTF-8");

    ChatbotSessionBean chatbotSessionBean = ChatbotRestoreConversationJerseyClient.getMyeSession("session_01");

    response.getWriter().print("Session ID: " + chatbotSessionBean.getSessionId() + "\n");
    response.getWriter().print("User ID: " + chatbotSessionBean.getUserId() + "\n");
    response.getWriter().print("Timestamp ID: " + chatbotSessionBean.getTimestamp() + "\n");
  }
}