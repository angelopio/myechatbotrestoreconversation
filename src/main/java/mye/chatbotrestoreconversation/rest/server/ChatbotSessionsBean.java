package mye.chatbotrestoreconversation.rest.server;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "chatbotSessionsBean")
@XmlAccessorType (XmlAccessType.FIELD)
public class ChatbotSessionsBean {

    @XmlElement(name="chatbotSessionBean")
    private List<ChatbotSessionBean> chatbotSessionsBean;

    public List<ChatbotSessionBean> getEmployeeList() {
        return chatbotSessionsBean;
    }

    public void setEmployeeList(List<ChatbotSessionBean> chatbotSessionsBean) {
        this.chatbotSessionsBean = chatbotSessionsBean;
    }
}