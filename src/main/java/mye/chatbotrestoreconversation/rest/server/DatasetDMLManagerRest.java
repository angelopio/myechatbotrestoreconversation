package mye.chatbotrestoreconversation.rest.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

// Class will be at addressable at the URI "/{sessionId}"
@Path("/{sessionId}")
public class DatasetDMLManagerRest {

	@GET // The java method will process HTTP GET requests
	@Produces(MediaType.APPLICATION_XML) // The Java method will produce content identified by the MIME Media type APPLICATION_XML
	public Response getChatbotSession(@PathParam("sessionId") String sessionId) throws InterruptedException, IOException {

		// Load credentials from JSON key file.
		// If you can't set the GOOGLE_APPLICATION_CREDENTIALS environment variable,
		// you can explicitly load the credentials file to construct the credentials.
		GoogleCredentials credentials;
		File credentialsPath = new File("WEB-INF/classes/mye-chatbot-restoreconv-6d9793e4945a.json");
		try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
			credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
		}

		BigQuery bigquery = BigQueryOptions.newBuilder().setProjectId("mye-chatbot-restoreconv").setCredentials(credentials).build().getService();

		// Use standard SQL syntax for queries. See:
		// https://cloud.google.com/bigquery/sql-reference/
		QueryJobConfiguration queryConfig = QueryJobConfiguration
				.newBuilder("SELECT sessionId, userId, timestamp, query FROM `mye-chatbot-restoreconv.myedataset_sessions.mye_sessions`"
						+ " WHERE sessionId = '" + sessionId + "'")
				.setUseLegacySql(false).build();

		// Create a job ID so that we can safely retry.
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

		// Wait for the query to complete.
		queryJob = queryJob.waitFor();

		// Check for errors
		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			// You can also look at queryJob.getStatus().getExecutionErrors() for all
			// errors, not just the latest one.
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		// Get the results.
		TableResult result = queryJob.getQueryResults();
		FieldValueList row = result.iterateAll().iterator().next(); // La query preleverà un solo record

		ChatbotSessionBean chatbotSessionsBean = new ChatbotSessionBean();

		chatbotSessionsBean.setSessionId(row.get("sessionId").getStringValue());
		chatbotSessionsBean.setUserId(row.get("userId").getStringValue());
		chatbotSessionsBean.setTimestamp(Long.parseLong(row.get("timestamp").getStringValue().replaceAll(".0", "")));
		chatbotSessionsBean.setQuery(row.get("query").getStringValue());

		// Build entity object for response REST
	    GenericEntity<ChatbotSessionBean> entity = new GenericEntity<ChatbotSessionBean>(chatbotSessionsBean, ChatbotSessionBean.class);
	    return Response.ok().entity(entity).build();
	}
}