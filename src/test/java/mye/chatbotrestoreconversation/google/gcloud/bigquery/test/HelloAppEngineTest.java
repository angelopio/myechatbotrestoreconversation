package mye.chatbotrestoreconversation.google.gcloud.bigquery.test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import mye.chatbotrestoreconversation.helloworld.HelloWorldServlet;

public class HelloAppEngineTest {

  @Test
  public void test() throws IOException {
	  
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    new HelloWorldServlet().doGet(null, response);
    Assert.assertEquals("text/plain", response.getContentType());
    Assert.assertEquals("UTF-8", response.getCharacterEncoding());
    Assert.assertEquals("Hello App Engine!\r\n", response.getWriterContent().toString());
  }
}
